<?php

/**
 * @file
 * Form API backports.
 */

use Drupal\Core\Form\FormState;

/**
 * Converts a Drupal 7 form_state array into a proper FormState object.
 *
 * @param array $form_state
 *   A form state array.
 * @param array $complete_form
 *   The complete form, passed by reference.
 *
 * @return \Drupal\Core\Form\FormState
 *   A new FormState object.
 */
function _backport_form_state(array &$form_state, array &$complete_form = []) {
  $new_form_state = new FormState();
  $new_form_state->setBuildInfo($form_state['build_info']);
  $new_form_state->setCompleteForm($complete_form);
  return $new_form_state;
}
