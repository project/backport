<?php

/**
 * @file
 * Database API backports.
 */

use Drupal\Core\Site\Settings;

/**
 * Sets a session variable specifying the lag time for ignoring a replica
 * server (A replica server is traditionally referred to as
 * a "slave" in database server documentation).
 * @see https://www.drupal.org/node/2275877
 */
function db_ignore_replica() {
  $connection_info = Database::getConnectionInfo();
  // Only set ignore_replica_server if there are replica servers being used,
  // which is assumed if there are more than one.
  if (count($connection_info) > 1) {
    // Five minutes is long enough to allow the replica to break and resume
    // interrupted replication without causing problems on the Drupal site from
    // the old data.
    $duration = Settings::get('maximum_replication_lag', 300);
    // Set session variable with amount of time to delay before using replica.
    $_SESSION['ignore_replica_server'] = REQUEST_TIME + $duration;
  }
}
