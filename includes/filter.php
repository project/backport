<?php

/**
 * @file
 * Filter module API backports.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\plus\Utility\Unicode;

/**
 * Implements hook_filter_info_alter().
 */
function backport_filter_info_alter(&$info) {
  $container = backport_container();
  if (!$container) {
    return;
  }

  // Don't backport core filters.
  $existing_filters = [];
  if (!variable_get('backport_core_filters', FALSE)) {
    $existing_filters = array_flip(array_merge(array_keys($info), ['filter_null', 'filter_align', 'filter_caption']));
  }

  // Extract just contrib D8 filters.
  $filters = array_diff_key($container->get('plugin.manager.filter')->getDefinitions(), $existing_filters);
  foreach ($filters as $plugin_id => $definition) {
    $info[$plugin_id] = [
      'title' => $definition['title'],
      'description' => $definition['description'],
      'prepare callback' => '_backport_filter_prepare',
      'process callback' => '_backport_filter_process',
      'settings callback' => '_backport_filter_settings',
      'tips callback' => '_backport_filter_tips',
      'default settings' => $definition['settings'],
      'module' => 'backport',
      'weight' => $definition['weight'],
    ];
  }
}

/**
 * Retrieves the Drupal 8 filter plugin from an existing filter object.
 *
 * @param \stdClass $filter
 *   A Drupal 7 filter plain object.
 *
 * @return \Drupal\filter\Plugin\FilterInterface
 */
function _backport_filter(\stdClass $filter) {
  $filters = backport_container()->get('plugin.manager.filter');
  $definition = $filters->getDefinition($filter->name);
  return $filters->createInstance($filter->name, [
    'status' => $filter->status,
    'weight' => $filter->weight,
    'settings' => NestedArray::mergeDeep($definition['settings'], $filter->settings),
  ]);
}

/**
 * Implements callback_filter_prepare().
 *
 * {@inheritdoc}
 */
function _backport_filter_prepare($text, $filter, $format, $langcode, $cache, $cache_id) {
  return _backport_filter($filter)->prepare($text, $langcode);
}

/**
 * Implements callback_filter_process().
 *
 * {@inheritdoc}
 */
function _backport_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  // Drupal 8 returns a FilterProcessResult object, typecast to a string.
  return (string) _backport_filter($filter)->process($text, $langcode);
}

/**
 * Implements callback_filter_settings().
 *
 * {@inheritdoc}
 */
function _backport_filter_settings($form, &$form_state, $filter, $format, $defaults, $filters) {
  return _backport_filter($filter)->settingsForm([], _backport_form_state($form_state, $form));
}

/**
 * Implements callback_filter_tips().
 *
 * {@inheritdoc}
 */
function _backport_filter_tips($filter, $format, $long) {
  return _backport_filter($filter)->tips($long);
}

/**
 * Process callback for local image filter.
 *
 * Note: this is needed because the D8 Filter module can't actually be loaded.
 */
function _filter_html_image_secure_process($text) {
  // Find the path (e.g. '/') to Drupal root.
  $base_path = base_path();
  $base_path_length = Unicode::strlen($base_path);

  // Find the directory on the server where index.php resides.
  $local_dir = \Drupal::root() . '/';

  $html_dom = Html::load($text);
  $images = $html_dom->getElementsByTagName('img');
  foreach ($images as $image) {
    $src = $image->getAttribute('src');
    // Transform absolute image URLs to relative image URLs: prevent problems on
    // multisite set-ups and prevent mixed content errors.
    $image->setAttribute('src', file_url_transform_relative($src));

    // Verify that $src starts with $base_path.
    // This also ensures that external images cannot be referenced.
    $src = $image->getAttribute('src');
    if (Unicode::substr($src, 0, $base_path_length) === $base_path) {
      // Remove the $base_path to get the path relative to the Drupal root.
      // Ensure the path refers to an actual image by prefixing the image source
      // with the Drupal root and running getimagesize() on it.
      $local_image_path = $local_dir . Unicode::substr($src, $base_path_length);
      $local_image_path = rawurldecode($local_image_path);
      if (@getimagesize($local_image_path)) {
        // The image has the right path. Erroneous images are dealt with below.
        continue;
      }
    }
    // Allow modules and themes to replace an invalid image with an error
    // indicator. See filter_filter_secure_image_alter().
    \Drupal::moduleHandler()->alter('filter_secure_image', $image);
  }
  $text = Html::serialize($html_dom);
  return $text;
}
