<?php

/**
 * @file
 * Module API backports.
 */

/**
 * Implements hook_module_implements_alter().
 */
function backport_module_implements_alter(&$implementations, $hook) {
  // Ensure backport is always at the beginning of hook_boot() and hook_init().
  if ($hook === 'boot' || $hook === 'init') {
    $groups = [];
    // However, ensure backport is loaded after dependencies.
    foreach (['any', 'backport_composer', 'composer_manager', 'backport'] as $module) {
      if (isset($implementations[$module])) {
        $groups[$module] = $implementations[$module];
        unset($implementations[$module]);
      }
    }
    $implementations = $groups + $implementations;
  }
}

/**
 * Implements hook_modules_installed().
 *
 * {@inheritdoc}
 */
function backport_modules_installed($modules) {
  // Invalidate the container.
  if ($kernel = backport_kernel()) {
    $kernel->rebuildContainer();
    $kernel->getContainer()->get('module_installer')->install($modules);
  }
}

/**
 * Implements hook_modules_uninstalled().
 *
 * {@inheritdoc}
 */
function backport_modules_uninstalled($modules) {
  // Invalidate the container.
  if ($kernel = backport_kernel()) {
    $kernel->getContainer()->get('module_installer')->uninstall($modules);
    $kernel->invalidateContainer();
  }
}

/**
 * Sets weight of a particular module.
 *
 * The weight of uninstalled modules cannot be changed.
 *
 * @param string $module
 *   The name of the module (without the .module extension).
 * @param int $weight
 *   An integer representing the weight of the module.
 */
function module_set_weight($module, $weight) {
  $extension_config = \Drupal::configFactory()->getEditable('core.extension');
  if ($extension_config->get("module.$module") !== NULL) {
    // Pre-cast the $weight to an integer so that we can save this without using
    // schema. This is a performance improvement for module installation.
    $extension_config
      ->set("module.$module", (int) $weight)
      ->set('module', module_config_sort($extension_config->get('module')))
      ->save(TRUE);

    // Prepare the new module list, sorted by weight, including filenames.
    // @see \Drupal\Core\Extension\ModuleInstaller::install()
    $module_handler = \Drupal::moduleHandler();
    $current_module_filenames = $module_handler->getModuleList();
    $current_modules = array_fill_keys(array_keys($current_module_filenames), 0);
    $current_modules = module_config_sort(array_merge($current_modules, $extension_config->get('module')));
    $module_filenames = [];
    foreach ($current_modules as $name => $weight) {
      $module_filenames[$name] = $current_module_filenames[$name];
    }
    // Update the module list in the extension handler.
    $module_handler->setModuleList($module_filenames);
    return;
  }
}

/**
 * Sorts the configured list of enabled modules.
 *
 * The list of enabled modules is expected to be ordered by weight and name.
 * The list is always sorted on write to avoid the overhead on read.
 *
 * @param array $data
 *   An array of module configuration data.
 *
 * @return array
 *   An array of module configuration data sorted by weight and name.
 */
function module_config_sort($data) {
  // PHP array sorting functions such as uasort() do not work with both keys and
  // values at the same time, so we achieve weight and name sorting by computing
  // strings with both information concatenated (weight first, name second) and
  // use that as a regular string sort reference list via array_multisort(),
  // compound of "[sign-as-integer][padded-integer-weight][name]"; e.g., given
  // two modules and weights (spaces added for clarity):
  // - Block with weight -5: 0 0000000000000000005 block
  // - Node  with weight  0: 1 0000000000000000000 node
  $sort = [];
  foreach ($data as $name => $weight) {
    // Prefix negative weights with 0, positive weights with 1.
    // +/- signs cannot be used, since + (ASCII 43) is before - (ASCII 45).
    $prefix = (int) ($weight >= 0);
    // The maximum weight is PHP_INT_MAX, so pad all weights to 19 digits.
    $sort[] = $prefix . sprintf('%019d', abs($weight)) . $name;
  }
  array_multisort($sort, SORT_STRING, $data);
  return $data;
}
