<?php

use Backport\Core\DrupalKernel;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\Request;

/**
 * Retrieves Composer's autoloader.
 *
 * @return \Composer\Autoload\ClassLoader|false
 *   Composer's autoloader or FALSE if none could be found.
 */
function _backport_get_composer_autoloader() {
  static $autoloader;
  if (!isset($autoloader)) {
    try {
      if (function_exists('backport_composer_autoloader')) {
        $autoloader = backport_composer_autoloader();
      }
      elseif (function_exists('composer_manager_vendor_dir')) {
        // Use require, not require_once (which returns TRUE if already loaded).
        $autoloader = require composer_manager_vendor_dir() . '/autoload.php';
      }
      _backport_validate_class_loader($autoloader);
    }
    catch (\Exception $e) {
      $autoloader = FALSE;
      trigger_error($e->getMessage(), E_USER_ERROR);
    }
  }
  return $autoloader;
}

/**
 * Validates that a ClassLoader type object has the necessary methods.
 *
 * Note: this is necessary since many of Symfony's autoloaders do not extend
 * from Composer's Class Loader, nor do they implement any sort of interface.
 *
 * @param object $autoloader
 *   A ClassLoader type of object.
 *
 * @throws \Exception
 *   When the provided $autoloader is not valid.
 */
function _backport_validate_class_loader($autoloader = NULL) {
  $valid = is_object($autoloader);
  if ($valid) {
    foreach (['findFile', 'loadClass'] as $method) {
      if (!method_exists($autoloader, $method)) {
        $valid = FALSE;
        break;
      }

    }
  }
  if (!$valid) {
    throw new \Exception('Backport requires a valid class loader.');
  }
}

/**
 * Implements hook_boot().
 */
function backport_boot() {
  backport_kernel();
}

/**
 * Retrieves the Symfony container.
 *
 * @return \Symfony\Component\DependencyInjection\ContainerInterface|false
 *   The Symfony container or FALSE if unavailable.
 */
function backport_container() {
  if ($kernel = backport_kernel()) {
    return $kernel->getContainer();
  }
  return FALSE;
}

/**
 * Retrieves Drupal 8's DrupalKernel for Drupal 7.
 *
 * @return \Backport\Core\DrupalKernel|false|null
 *   The Drupal 7 Kernel, FALSE if an Composer Autoloader could not be found or
 *   NULL if not yet initialized.
 */
function backport_kernel() {
  static $kernel;
  if ($kernel === NULL) {
    $kernel = FALSE;

    // In the event that certain functions are not available, they must be
    // manually loaded to ensure the container can be rebuilt if needed.
    if (!function_exists('drupal_get_path')) {
      require_once DRUPAL_ROOT . '/includes/common.inc';
    }

    try {
      if (($autoloader = _backport_get_composer_autoloader()) && class_exists('Drupal\Core\DrupalKernel')) {
        $request = Request::createFromGlobals();
        $kernel = DrupalKernel::createFromRequest($request, $autoloader, 'prod');
        $kernel->prepareLegacyRequest($request);
      }
    }
    catch (\Exception $e) {
      $kernel = FALSE;
      trigger_error($e->getMessage(), E_USER_ERROR);
    }
  }
  return $kernel;
}

/**
 * Implements hook_init().
 */
function backport_init() {
  global $theme_key;
  try {
    // Retrieve the active theme using the global $theme_key variable. This is
    // the first available opportunity to do this since hook_init() is invoked
    // directly after drupal_theme_initialize().
    if ($container = backport_container()) {
      $active_theme = $container->get('theme.initialization')->getActiveThemeByName($theme_key);

      // ThemeManager only sets the ActiveTheme after it has been determined via
      // RouteMatcher. Since routes do not work in Drupal 7, it must be manually
      // set here.
      $container->get('theme.manager')->setActiveTheme($active_theme);
    }
  }
  catch (\Exception $e) {
    trigger_error($e->getMessage(), E_USER_ERROR);
  }
}

/**
 * Implements hook_registry_files_alter().
 *
 * Invalidate the container. This cannot be done in hook_flush_caches() since
 * that is invoked during cron runs which would defeat the whole purpose of a
 * cached container if it was constantly being invalidated and rebuilt.
 *
 * Instead, use this hook which is invoked when registry_rebuild() is invoked.
 * Thus, this hook is invoked when drupal_flush_all_caches() is invoked.
 *
 * {@inheritdoc}
 */
function backport_registry_files_alter(&$files, $modules) {
  if ($kernel = backport_kernel()) {
    $kernel->invalidateContainer();

    // Delete all items for each cache bin.
    foreach (Cache::getBins() as $bin => $cache_backend) {
      // Skip specific bins that haven't been upgraded to 8.x.
      if (in_array($bin, ['entity', 'menu'], TRUE)) {
        continue;
      }
      $cache_backend->deleteAll();
    }
  }
  // In the event that no kernel was returned, attempt to manually rebuild.
  // This is analogous to Drush's `cache-rebuild` for 8.x sites.
  else {
    try {
      if (function_exists('apc_fetch')) {
        apc_clear_cache('user');
      }

      $request = Request::createFromGlobals();
      $site_path = DrupalKernel::findSitePath($request);
      Settings::initialize(DRUPAL_ROOT, $site_path, $autoloader);

      // Bootstrap up to where caches exist and clear them.
      $kernel = new DrupalKernel('prod', $autoloader);
      $kernel->setSitePath(DrupalKernel::findSitePath($request));

      // Invalidate the container.
      $kernel->invalidateContainer();

      // Prepare a NULL request.
      $kernel->prepareLegacyRequest($request);
    }
    catch (\Exception $e) {
      trigger_error($e->getMessage(), E_USER_ERROR);
    }
  }
}
