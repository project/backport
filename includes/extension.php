<?php

/**
 * @file
 * Extension API backports.
 */

use Drupal\Core\Extension\Extension;

/**
 * Helper function for determining if an extension is installed.
 *
 * @param string $type
 *   The type of extensions to retrieve.
 * @param string $name
 *   The name of the extension $type to retrieve.
 *
 * @return bool
 *   TRUE or FALSE
 */
function backport_extension_installed($type, $name) {
  if ($type === 'module') {
    $type = 'module_enabled';
  }
  $file = backport_system_list($type, $name);
  return $file && $file->status;
}

/**
 * Wrapper for system_list() to covert into Extension objects.
 *
 * @param string $type
 *   The type of extensions to retrieve.
 * @param string $name
 *   Optional. A specific extension of $type to retrieve.
 *
 * @return \Drupal\Core\Extension\Extension[]|\Drupal\Core\Extension\Extension|null
 *   An array of Extension objects if $name wasn't provided. If $name was
 *   provide, a single Extension object or NULL if it doesn't exist.
 */
function backport_extension_list($type, $name = NULL) {
  $listings = backport_system_list($type, $name);
  if (isset($name)) {
    $listings = [$listings];
  }
  foreach ($listings as $key => $listing) {
    $listings[$key] = new Extension(DRUPAL_ROOT, $type, $listing->pathname, $listing->filename);
    if ($type === 'theme') {
      $listings[$key]->engine = $listing->engine;
      $listings[$key]->extension = $listing->extension;
      $listings[$key]->owner = $listing->owner;
    }
  }
  if (isset($name)) {
    return reset($listings) ?: NULL;
  }
  return $listings;
}

/**
 * Wrapper for system_list() so individual extensions can be retrieved.
 *
 * @param string $type
 *   The type of extensions to retrieve.
 * @param string $name
 *   Optional. A specific extension of $type to retrieve.
 *
 * @return \stdClass|\stdClass[]
 *   A specific plain object of extension information or an array of objects.
 *
 * @todo Revisit in 8.6.x to see if this will still be needed.
 * @see https://www.drupal.org/node/2709919
 */
function backport_system_list($type, $name = NULL) {
  $listings = system_list($type);
  foreach ($listings as $key => $listing) {
    if (!isset($listing->pathname)) {
      $listing->pathname = $listing->filename;
    }
    if ($type === 'theme') {
      if (!isset($listing->engine)) {
        $parts = explode('/', $listing->owner);
        $listing->engine = basename(array_pop($parts), '.engine');
      }
      if (!isset($listing->extension)) {
        $listing->extension = $listing->engine === 'twig' ? 'html.twig' : 'tpl.php';
      }
    }
  }
  if (isset($name)) {
    return isset($listings[$name]) ? $listings[$name] : NULL;
  }
  return $listings;
}
