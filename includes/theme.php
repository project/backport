<?php

/**
 * @file
 * Theme API backports.
 */

/**
 * Implements hook_themes_disabled().
 */
function backport_themes_disabled() {
  // Invalidate the container.
  if ($kernel = backport_kernel()) {
    $kernel->invalidateContainer();
  }
}

/**
 * Implements hook_themes_enabled().
 */
function backport_themes_enabled() {
  // Invalidate the container.
  if ($kernel = backport_kernel()) {
    $kernel->invalidateContainer();
  }
}
