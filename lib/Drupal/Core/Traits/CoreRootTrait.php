<?php

namespace Backport\Core\Traits;

trait CoreRootTrait {

  /**
   * The core root path.
   *
   * @var string
   */
  protected static $coreRoot;

  /**
   * Retrieves the core root.
   *
   * @return string
   *   The core root.
   */
  protected function coreRoot() {
    if (!isset(static::$coreRoot)) {
      static::$coreRoot = (string) \Drupal::service('core.root');
    }
    return static::$coreRoot;
  }

}
