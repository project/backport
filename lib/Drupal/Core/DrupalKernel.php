<?php

namespace Backport\Core;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\YamlFileLoader;
use Drupal\Core\DrupalKernel as CoreDrupalKernel;
use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\File\MimeType\MimeTypeGuesser;
use Drupal\Core\Language\Language;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\TerminableInterface;

/**
 * {@inheritdoc}
 *
 * Note: this is a Drupal 7 specific "kernel" used primarily to assist with
 * the construction and instantiation of the container. It does not "bootstrap"
 * Drupal like in Drupal 8.
 */
class DrupalKernel extends CoreDrupalKernel implements DrupalKernelInterface, TerminableInterface {

  /**
   * {@inheritdoc}
   *
   * @deprecated The environment is always initialized in Drupal 7.
   */
  protected static $isEnvironmentInitialized = TRUE;

  /**
   * Drupal 8's Core Root directory.
   *
   * This is not the same as "appRoot" which is really DRUPAL_ROOT or DOCROOT.
   *
   * This is where composer installed the "drupal/core" package, likely in the
   * vendor directory.
   *
   * @var string
   */
  protected $coreRoot;

  /**
   * {@inheritdoc}
   */
  public function __construct($environment, $class_loader, $allow_dumping = TRUE, $app_root = NULL) {
    parent::__construct($environment, $class_loader, $allow_dumping, $app_root);

    // Determine where the "drupal/core" root path is based on parent class.
    $this->coreRoot = dirname(dirname(dirname(dirname($this->classLoader->findFile(get_parent_class($this))))));
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated This method does nothing in Drupal 7.
   */
  public static function bootEnvironment($app_root = NULL) {
    // Intentionally left blank, Drupal 7 has already bootstrapped.
  }

  /**
   * {@inheritdoc}
   */
  protected function compileContainer() {
    // We are forcing a container build so it is reasonable to assume that the
    // calling method knows something about the system has changed requiring the
    // container to be dumped to the filesystem.
    if ($this->allowDumping) {
      $this->containerNeedsDumping = TRUE;
    }

    $this->initializeServiceProviders();
    $container = $this->getContainerBuilder();
    $container->set('kernel', $this);
    $container->setParameter('container.modules', $this->getModulesParameter());
    $container->setParameter('install_profile', $this->getInstallProfile());

    // Get a list of namespaces and put it onto the container.
    $namespaces = $this->getModuleNamespacesPsr4($this->getModuleFileNames());
    // Add all components in \Drupal\Core and \Drupal\Component that have one of
    // the following directories:
    // - Element
    // - Entity
    // - Plugin
    foreach (['Core', 'Component'] as $parent_directory) {
      $path = str_replace(DRUPAL_ROOT . '/', '', $this->coreRoot) . '/' . 'lib/Drupal/' . $parent_directory;
      $parent_namespace = 'Drupal\\' . $parent_directory;
      foreach (new \DirectoryIterator($path) as $component) {
        /** @var $component \DirectoryIterator */
        $pathname = $component->getPathname();
        if (!$component->isDot() && $component->isDir() && (
            is_dir($pathname . '/Plugin') ||
            is_dir($pathname . '/Entity') ||
            is_dir($pathname . '/Element')
          )) {
          $namespaces[$parent_namespace . '\\' . $component->getFilename()] = $path . '/' . $component->getFilename();
        }
      }
    }
    $container->setParameter('container.namespaces', $namespaces);

    // Store the default language values on the container. This is so that the
    // default language can be configured using the configuration factory. This
    // avoids the circular dependencies that would created by
    // \Drupal\language\LanguageServiceProvider::alter() and allows the default
    // language to not be English in the installer.
    $default_language_values = Language::$defaultValues;
    if ($system = $this->getConfigStorage()->read('system.site')) {
      if ($default_language_values['id'] != $system['langcode']) {
        $default_language_values = ['id' => $system['langcode']];
      }
    }
    $container->setParameter('language.default_values', $default_language_values);

    // Register synthetic services.
    $container->register('class_loader')->setSynthetic(TRUE);
    $container->register('kernel', 'Symfony\Component\HttpKernel\KernelInterface')->setSynthetic(TRUE);
    $container->register('service_container', 'Symfony\Component\DependencyInjection\ContainerInterface')->setSynthetic(TRUE);

    // Register application services.
    $yaml_loader = new YamlFileLoader($container);
    foreach ($this->serviceYamls['app'] as $filename) {
      $yaml_loader->load($filename);
    }
    foreach ($this->serviceProviders['app'] as $provider) {
      if ($provider instanceof ServiceProviderInterface) {
        $provider->register($container);
      }
    }
    // Register site-specific service overrides.
    foreach ($this->serviceYamls['site'] as $filename) {
      $yaml_loader->load($filename);
    }
    foreach ($this->serviceProviders['site'] as $provider) {
      if ($provider instanceof ServiceProviderInterface) {
        $provider->register($container);
      }
    }

    // Identify all services whose instances should be persisted when rebuilding
    // the container during the lifetime of the kernel (e.g., during a kernel
    // reboot). Include synthetic services, because by definition, they cannot
    // be automatically reinstantiated. Also include services tagged to persist.
    $persist_ids = [];
    foreach ($container->getDefinitions() as $id => $definition) {
      // It does not make sense to persist the container itself, exclude it.
      if ($id !== 'service_container' && ($definition->isSynthetic() || $definition->getTag('persist'))) {
        $persist_ids[] = $id;
      }
    }
    $container->setParameter('persist_ids', $persist_ids);

    $container->compile();
    return $container;
  }

  /**
   * {@inheritdoc}
   */
  public function discoverServiceProviders() {
    $this->serviceYamls = [
      'app' => [],
      'site' => [],
    ];
    $this->serviceProviderClasses = [
      'app' => [],
      'site' => [],
    ];
    $this->serviceYamls['app']['core'] = $this->coreRoot . '/core.services.yml';
    $this->serviceYamls['app']['backport'] = \drupal_get_path('module', 'backport') . '/backport.services.yml';
    $this->serviceProviderClasses['app']['core'] = 'Backport\Core\CoreServiceProvider';

    // Retrieve enabled modules and register their namespaces.
    if (!isset($this->moduleList)) {
      $modules = [];
      foreach (system_list('module_enabled') as $file) {
        $modules[$file->name] = (int) $file->weight;
      }
      $this->moduleList = $modules;
    }
    $module_filenames = $this->getModuleFileNames();
    $this->classLoaderAddMultiplePsr4($this->getModuleNamespacesPsr4($module_filenames));

    // Load each module's serviceProvider class.
    foreach ($module_filenames as $module => $filename) {
      // Skip Backport module since this is added directly after core's above.
      if ($module === 'backport') {
        continue;
      }
      $camelized = ContainerBuilder::camelize($module);
      $name = "{$camelized}ServiceProvider";
      $class = "Drupal\\{$module}\\{$name}";
      if (class_exists($class)) {
        $this->serviceProviderClasses['app'][$module] = $class;
      }
      $filename = dirname($filename) . "/$module.services.yml";
      if (file_exists($filename)) {
        $this->serviceYamls['app'][$module] = $filename;
      }
    }

    // Add site-specific service providers.
    if (!empty($GLOBALS['conf']['container_service_providers'])) {
      foreach ($GLOBALS['conf']['container_service_providers'] as $class) {
        if ((is_string($class) && class_exists($class)) || (is_object($class) && ($class instanceof ServiceProviderInterface || $class instanceof ServiceModifierInterface))) {
          $this->serviceProviderClasses['site'][] = $class;
        }
      }
    }
    $this->addServiceFiles(Settings::get('container_yamls', []));
  }

  /**
   * Retrieves the core root directory.
   *
   * @return string
   *   The core root directory.
   */
  public function getCoreRoot() {
    return $this->coreRoot;
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleNamespacesPsr4($module_file_names) {
    $namespaces = [];
    foreach ($module_file_names as $module => $filename) {
      $core_path = $this->coreRoot . "/modules/$module/src";
      if (file_exists($core_path)) {
        $namespaces["Drupal\\$module"] = str_replace(DRUPAL_ROOT . '/', '', $core_path);
      }
      else {
        $namespaces["Drupal\\$module"] = dirname($filename) . '/src';
      }
    }
    return $namespaces;
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated This method returns existing DRUPAL_ROOT constant in Drupal 7.
   */
  protected static function guessApplicationRoot() {
    return DRUPAL_ROOT;
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated This method is not supported in Drupal 7
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    throw new \Exception('This method is not supported in Drupal 7');
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated This method does nothing in Drupal 7.
   */
  protected function initializeRequestGlobals(Request $request) {
    // Intentionally left blank, Drupal 7 has already bootstrapped.
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated This method does nothing in Drupal 7.
   */
  public function loadLegacyIncludes() {
    // Intentionally left blank, Drupal 7 has already bootstrapped.
  }

  /**
   * {@inheritdoc}
   */
  public function preHandle(Request $request) {
    // Put the request on the stack.
    $this->container->get('request_stack')->push($request);

    // Set the allowed protocols.
    UrlHelper::setAllowedProtocols($this->container->getParameter('filter_protocols'));

    // Override of Symfony's MIME type guesser singleton.
    MimeTypeGuesser::registerWithSymfonyGuesser($this->container);

    $this->prepared = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function shutdown() {
    if (FALSE === $this->booted) {
      return;
    }
    $this->booted = FALSE;
    $this->container = NULL;
    $this->moduleList = NULL;
    $this->moduleData = [];
  }

}
