<?php

namespace Backport\Core;

use Drupal\Core\CoreServiceProvider as DrupalCoreServiceProvider;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

/**
 * {@inheritdoc}
 */
class CoreServiceProvider extends DrupalCoreServiceProvider {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    // Add filter plugin manager.
    if (backport_extension_installed('module', 'filter')) {
      $definition = new DefinitionDecorator('default_plugin_manager');
      $definition->setClass('Drupal\filter\FilterPluginManager');
      $container->setDefinition('plugin.manager.filter', $definition);
    }
  }

}
