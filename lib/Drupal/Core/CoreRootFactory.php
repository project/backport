<?php

namespace Backport\Core;

use Drupal\Core\DrupalKernelInterface;

/**
 * Gets the core root from the kernel.
 */
class CoreRootFactory {

  /**
   * The Drupal kernel.
   *
   * @var \Backport\Core\DrupalKernel
   */
  protected $drupalKernel;

  /**
   * Constructs an CoreRootFactory instance.
   *
   * @param \Drupal\Core\DrupalKernelInterface $drupal_kernel
   *   The Drupal kernel.
   */
  public function __construct(DrupalKernelInterface $drupal_kernel) {
    $this->drupalKernel = $drupal_kernel;
  }

  /**
   * Gets the app root.
   *
   * @return string
   */
  public function get() {
    return $this->drupalKernel->getCoreRoot();
  }

}
